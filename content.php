<article class="main-content-article">
	<div class="narrow">
	<!-- End post-thumbnail Div -->
	<!-- Start main-content Div -->
	<div class="main-content row <?php if (has_post_thumbnail() && !is_archive() && !is_single()) { echo("has-thumbnail")?>">

		<!-- Start post-thumbnail Div -->
		<div class="post-thumbnail col-md-3">
			<a href="<?php echo get_the_permalink(); ?>"><?php //Add the thumbnails to posts
				the_post_thumbnail('small-thumbnail');
			?></a>
			<p class="post-info"><?php the_time('F jS, Y g:i a'); ?> | by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a> </p>
		</div>
	<?php } else if (!has_post_thumbnail() && !is_archive() && !is_single()) { ?>
		">		
		<div class="row col-md-3">
			<img class="shine-logo-img-blog" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/icons/ghostlyGamesLogoShine.svg" alt="Ghostly Games Entertainment"></img>
		<p class="post-info"><?php the_time('F jS, Y g:i a'); ?> | by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a> 
		</p>
		</div>
	<?php } else { ?> 
		">
	<?php }; ?>
		
		<?php 
		//Page selection for the type of content
		//First the search page
			if (is_search()){ ?>

			<div class="col-md-12">
				<div class="index-p">
					<p>
						<?php  the_excerpt(); ?>
						<a href="<?php the_permalink(); ?>" class="contr">Continue Reading&raquo</a>
					</p>
				</div>	
				</div>
			</div>
			<?php 
			//Is archive page
			} elseif (is_archive() OR is_single()){ ?>
				<?php //Add the thumbnails to posts
					the_post_thumbnail('banner-image');
				 ?>
				<!-- Start index-p Div -->

				<div class="col-md-12">
				<div class="index-p">
					<?php  
					the_content(); 
					//the_excerpt();
					?>
				</div>
				</div>
				<!-- End index-p Div -->
			</div>
			<!-- End main-content Div -->
			<?php } else {  
			//If your post has a WP inserted Excerpt applied
			if ($post->post_excerpt) { ?>
				<div class="col-md-9">
				<div class="os-animation" data-animation="fadeInUp">
					<h3 class="heading"><?php the_title()?></h3>
					<div class="heading-underline"></div>
				</div>
				<div class="index-p">
					<p>
						<?php  the_excerpt(); ?>
						<a href="<?php the_permalink(); ?>" class="contr">Continue Reading&raquo</a>
					</p>
					
				
				</div>
			<?php } 
			//If the content is larger than the custom excerpt lenght
			else if (word_count() > custom_excerpt_length()) { ?>
				<div class="col-md-9">
				<div class="os-animation" data-animation="fadeInUp">
					<h3 class="heading"><?php the_title()?></h3>
					<div class="heading-underline"></div>
				</div>
				<div class="index-p">
					<p>
						<?php  the_excerpt(); ?>
						<a href="<?php the_permalink(); ?>" class="contr">Continue Reading&raquo</a>
					</p>
					
				
				</div>
			<?php } else { ?>
				<div class="os-animation" data-animation="fadeInUp">
					<h3 class="heading"><?php the_title()?></h3>
					<div class="heading-underline"></div>
				</div>
				<div class="index-p">
					<?php  the_content(); ?>
				</div>
			<?php }; ?>
		</div>
	<!-- End main-content Div -->
		<?php } ?>

	</div>
</article>