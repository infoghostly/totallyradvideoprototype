<?php

get_header();

if(have_posts()) :
	while (have_posts()) : the_post(); ?>
		<article class="main-content-article container-fluid">
			<!-- Start main-content Div -->
			<div class="main-content narrow">
			<?php //get_template_part('nav-has-children');?>


				<!-- Start index-p Div -->
				<div class="index-p <?php echo strtolower(str_replace(' ', '', wp_title('', false)));?>">
					<?php  the_content(); ?>
				</div>
				<!-- End index-p Div -->
			</div>
			<!-- End main-content Div -->
		</article>
	<?php endwhile;
else :
	echo '<p>No content found </p>';
endif;

get_footer();
?>