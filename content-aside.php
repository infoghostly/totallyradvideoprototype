<article class="post post-aside-container">

	<!-- Start post-aside Div -->
	<div class="post-aside">
		<p class="mini-meta"><?php the_author(); ?> @ <?php the_time('F j, Y'); ?></p>
		<?php the_content(); ?>
	</div>
	<!-- End post-aside Div -->
</article>