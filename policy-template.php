<?php

/*
Template Name: Policy Page Layout
*/

get_header();

if(have_posts()) :
	while (have_posts()) : the_post(); ?>
		<article class="main-content-article container-fluid">
			<!-- Start main-content Div -->
			<div class="main-content narrow">
				<div class="col-12 os-animation" data-animation="fadeInUp">
					<h3 class="heading">Review Our Policies</h3>
					<div class="heading-underline"></div>
				</div>
				<!-- Start info-box Div -->
				<div class="policy-info-box mobile-off">
					<h4>Disclaimer</h4>
					<p> ALL CHARACTERS ARE LICENSED AND TRADEMARKED BY THEIR RESPECTIVE OWNERS. GHOSTLY GAMES ENTERTAINMENT AND IT'S AFFILIATES DO NOT MAKE ANY CLAIMS OF OWNERSHIP TO THOSE TRADEMARK CHARACTERS OR PRODUCTS. THIS WEBSITE AND THE SERVICE CONTAINS COPYRIGHT MATERIALS, TRADEMARKS, AND OTHER PROPRIETARY INFORMATION, INCLUDING, BUT NOT LIMITED TO SOFTWARE, PHOTOS, VIDEO, GRAPHICS, MUSIC, SOUND, AND THE ENTIRE CONTENTS OF THE SERVICE IS PROTECTED BY COPYRIGHT AS A COLLECTIVE WORK UNDER THE AUSTRALIAN COPYRIGHT LAWS.</p>
					<p>Copyright© 2017 Ghostly Games Entertainment</p>
				</div>
				<!-- End info-box Div -->
				<!-- Start index-p Div -->
				<div class="index-p">
					<?php  the_content(); ?>
				</div>
				<!-- End index-p Div -->
				<!-- Start info-box Div -->
				<div class="policy-info-box mobile-on">
					<h4>Disclaimer</h4>
					<p> ALL CHARACTERS ARE LICENSED AND TRADEMARKED BY THEIR RESPECTIVE OWNERS. GHOSTLY GAMES ENTERTAINMENT AND IT'S AFFILIATES DO NOT MAKE ANY CLAIMS OF OWNERSHIP TO THOSE TRADEMARK CHARACTERS OR PRODUCTS. THIS WEBSITE AND THE SERVICE CONTAINS COPYRIGHT MATERIALS, TRADEMARKS, AND OTHER PROPRIETARY INFORMATION, INCLUDING, BUT NOT LIMITED TO SOFTWARE, PHOTOS, VIDEO, GRAPHICS, MUSIC, SOUND, AND THE ENTIRE CONTENTS OF THE SERVICE IS PROTECTED BY COPYRIGHT AS A COLLECTIVE WORK UNDER THE AUSTRALIAN COPYRIGHT LAWS.</p>
					<p>Copyright© 2017 Ghostly Games Entertainment</p>
				</div>
				<!-- End info-box Div -->

			</div>
			<!-- End main-content Div -->
		</article>
	<?php endwhile;
else :
	echo '<p>No content found </p>';
endif;

get_footer();
?>