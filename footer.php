
<!-- Start Contact -->
<div class="offset">	

	<footer>
		<div class="row">
			<div class="col-md-5">
				<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/TR_Logo/totallyrad_logo-FINAL.png" alt="Totally Rad Video Games - Video Game Party - Laser Tag - Virtual Reality - Foam Parties"></a>
				<p>VIRTUAL REALITY GAMING, RACING SIMULATOR, FORTNITE, OCULUS, MOBILE LASER TAG AND FOAM PARTIES. BIRTHDAY – SCHOOL FUNCTIONS – CORPORATE EVENTS </p>
				<strong>Our Location</strong>
				<p>TR to complete</p>
				<strong>Contact Info</strong>
				<p>(323) 336-0714 <br>info@totallyradvideogames.com</p>
				<?php get_template_part('socialmedialinks');?>
			</div>
					<!--Start Contact Us Location - WP Plugin Contact 7 being used here -->
			<div id="contact" class="col-md-7">
						<!-- End footer-widgets Div -->
			<?php if(is_active_sidebar('footer1')) : ?>
				<!-- Start widget-footer-area Div -->
				<div class="contact-form">
					<?php dynamic_sidebar('footer1');  ?>
				</div>
				<!-- End widget-footer-area Div -->
			<?php endif; ?>
			</div>
			<hr class="socket">
			&copy; Totally Rad Video Games and Laser Tag
		</div><!-- End of Row -->
		<!--End Contact Us Location -->



	</footer><!-- End of Footer -->

</div>
<!-- Start floating-phone Div -->
<div class="floating-phone os-animation" data-animation="bounce">
	<a href="tel:0406763101"><img class="phone-icon" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/icons/red--phone.svg"></a>
</div>
		<!-- End floating-phone Div -->
<!-- End Contact  -->
	<?php wp_footer(); ?>
</body>
</html>