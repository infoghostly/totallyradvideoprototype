<?php 
@ini_set( 'upload_max_size' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'max_execution_time', '400' );
/**
 * way to enqueue scripts and styles
 */
define ('VERSION', '1.5398522443');

function version_id() {
		if ( true )
		//if ( WP_DEBUG )
			return time();
		return VERSION;
}

function wpdocs_theme_name_scripts() {

/*Test 5234*/

	/*The CSS import*/
    wp_enqueue_style( 'bootstrap_4.1.3_min', get_template_directory_uri() . '/css/bootstrap.min.css', '', version_id());
    wp_enqueue_style( 'animate_css', get_template_directory_uri() . '/css/animate.css', '', version_id());
    wp_enqueue_style( 'arrow_css', get_template_directory_uri() . '/css/arrow.css', '', version_id());
    wp_enqueue_style( 'fixed_css', get_template_directory_uri() . '/css/fixed.css', '', version_id());
    wp_enqueue_style( 'lightbox_css', get_template_directory_uri() . '/css/lightbox.css', '', version_id());
    wp_enqueue_style( 'owl_carousel_css', get_template_directory_uri() . '/css/owl.carousel.css', '', version_id());
    wp_enqueue_style( 'owl_theme_default_css', get_template_directory_uri() . '/css/owl.theme.default.css', '', version_id());
    wp_enqueue_style( 'waypoints_css', get_template_directory_uri() . '/css/waypoints.css', '', version_id());
    wp_enqueue_style( 'styles_nuno_theme', get_template_directory_uri() . '/css/styles_nuno_theme.css', '', version_id());
	/*Java JQuery import*/

    wp_enqueue_script( 'JQuery_js', get_template_directory_uri() . '/js/jquery-3.3.1.min.js', '',  version_id(), true);	
    wp_enqueue_script( 'fontawesome_cdn', 'https://use.fontawesome.com/releases/v5.8.2/js/all.js', '',  version_id(), true);
    wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/js/bootstrap.min.js', '',  version_id(), true);
    wp_enqueue_script( 'waypoints_js', get_template_directory_uri() . '/js/waypoints.js', '',  version_id(), true);
    wp_enqueue_script( 'contact_js', get_template_directory_uri() . '/js/contact.js', '',  version_id(), true);
    wp_enqueue_script( 'counterup_js', get_template_directory_uri() . '/js/jquery.counterup.js', '',  version_id(), true);
    wp_enqueue_script( 'jquery_waypoints_js', get_template_directory_uri() . '/js/jquery.waypoints.min.js', '',  version_id(), true);
    wp_enqueue_script( 'lightbox_js', get_template_directory_uri() . '/js/lightbox.js', '',  version_id(), true);
    wp_enqueue_script( 'owl_carousel_js', get_template_directory_uri() . '/js/owl.carousel.min.js', '',  version_id(), true);
    wp_enqueue_script( 'validator_js', get_template_directory_uri() . '/js/validator.js', '',  version_id(), true);

    /*Below is our CUSTOM JS*/
    wp_enqueue_script( 'scripts_bundled', get_template_directory_uri() . '/js/scripts-bundled.js', '',  version_id(), true);
    wp_enqueue_script( 'custom', get_template_directory_uri() . '/js/custom.js', '',  version_id(), true);
	

	if (is_page('faq')) {
    	wp_enqueue_script( 'faqjs', get_template_directory_uri() . '/js/toggleFaq.js', '',  version_id(),true);

	};
}

/*Perform the function by add action*/

/*to import css and js */
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

function last_scripts() {
	if (is_page('booking')) {
    	wp_enqueue_script( 'priceComboSelect', get_template_directory_uri() . '/js/priceComboSelect.js', '',  version_id(),true);
    }
}

add_action( 'wp_enqueue_scripts', 'last_scripts', PHP_INT_MAX );

//Get Top Ancestor
function get_top_ancestor_id(){
	global $post;
	if ($post->post_parent){
		$ancestors = array_reverse(get_post_ancestors($post->ID));
		return $ancestors[0];
	}

	return $post->ID;
}

//Does page have children
function has_children(){
	global $post;

	$pages = get_pages('child_of=' .$post->ID);
	return count($pages);
}

//Customize excerpt word count lenght
function custom_excerpt_length(){
	return 60;
}
add_filter('excerpt_length', 'custom_excerpt_length');

//Word count for posts
function word_count() {
    $content = get_post_field( 'post_content', $post->ID );
    $word_count = str_word_count( strip_tags( $content ) );
    return $word_count;
}

function GhostlyGamesWebSite_Setup(){
	//Need to Register Navigation Menus
	register_nav_menus(array(
		'primary' => __( 'Primary Menu'),
		'footer' => __( 'Footer')
	));
	//Add Feature image support
	add_theme_support('post-thumbnails');

	//Adding image size templates
	//Syntax ('name of size','width px','height px', '(hardcrope = true / softcrop = false)')
	//add_image_size('small-thumbnail', 300, 250, true);
	//add_image_size('banner-image', 1200, 350, true);
	add_image_size('small-thumbnail', 300, 250, false);
	add_image_size('banner-image', 1300, 400, true);

	//Add Post Format support
	add_theme_support('post-formats', array('aside', 'gallery', 'link'));
}

add_action('after_setup_theme', 'GhostlyGamesWebSite_Setup');


// Add Our Widget Locations
function ourWidgetsInit(){
	register_sidebar(array(
		'name' => 'Sidebar',
		'id' => 'sidebar1',
		'before_widget' => '<div class="widget-item">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-hfour">',
		'after_title' => '</h4>'
		));
	register_sidebar(array(
		'name' => 'Footer Area 1',
		'id' => 'footer1',
		'before_widget' => '<div class="widget-item">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-hfour">',
		'after_title' => '</h4>' 
		));

}

add_action('widgets_init','ourWidgetsInit');

?>