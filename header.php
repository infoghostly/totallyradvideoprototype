<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<link rel="shortcut icon" type="image/png" href="<?php echo esc_url(get_template_directory_uri()); ?>/img/favicon/TR_favicon-32x32.png"/>
		<?php wp_head(); ?>
	</head>

<!--May need to warp the data-spy to target only front page -NEED TO CHECK!!-->
<body <?php body_class(); ?> data-spy="scroll" data-target="#navbarResponsive">

	<!--Navigation Bar -->
	<nav class="navbar navbar-expand-lg fixed-top">
		<div class="container-fluid">
			<!--Nav Logo -->
			<a href="/" class="navbar-brand"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/TR_Logo/TotalyRad_Small_Logo.svg" alt="Video Game Party - Laser Tag - Virtual Reality - Foam Parties"></a>
			<!--Toggle button -->
			<button class="navbar-toggler site-header__menu-icon" type="button" data-toggle="collapse" data-target="#navbarResponsive">
		        <div class="custom-toggler-icon site-header__menu-icon__middle"></div>
		    </button>
			<!--Nav bar links - dynamic from WP -- requires NavMenuAddClassAndRoles for bootstrap classes-->
				<?php 
				$args = array(
				'container_class' => 'collapse navbar-collapse',
				'container_id' => 'navbarResponsive',
				'theme_location' => 'primary',
				'menu_class' => 'navbar-nav ml-auto zero-opacity'
			 	);
				wp_nav_menu($args); 

				?>
	
		</div>
	</nav> 
	<!--Navigation Bar -->
	<?php 
	$url = $_SERVER["REQUEST_URI"];

	$isItLaserTag = strpos($url, 'lasertag');
	$isOurGameTrailer = strpos($url, 'our-game-trailer');

	if (is_front_page()) { ?>
	<!-- Start Home -->
	<div id="home">
		<!--- Start Carousel Image Slider -->
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="0">
			<ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner" role="listbox">


				<!-- Slide One -->
				<div class="carousel-item background-1 active" alt="">
					<div class="dark-overlay-2 os-animation" data-animation="slideInDown" delay=".4s"></div>
					<div class="carousel-caption text-center">
						<div class="os-animation" data-animation="bounceInUp" data-delay=".6s">
							<img class="carousel-caption-logo" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/TR_Logo/totallyrad_logo-FINAL.png" alt="Video Game Party - Laser Tag - Virtual Reality - Foam Parties">
						</div>
						<div class="os-animation" data-animation="bounceInUp" data-delay=".8s">
							<h3>Video Game Truck, Mobile Laser Tag<br> and Foam Parties</h3>
						</div>		
						<div class="os-animation" data-animation="bounceInUp" data-delay="1s">
							<a class="btn btn-outline-light btn-lg" href="#features">Learn More</a>
						</div>
					</div> <!--- End Carousel Caption -->
				</div>
				<!-- Slide Two --> 
				<div class="carousel-item background-2" alt="">
					<div class="dark-overlay os-animation" data-animation="slideInUp" delay=".4s"></div>
					<div class="carousel-caption text-center carousel-caption-no-logo">
						<div class="os-animation" data-animation="bounceInUp" data-delay=".8s">
							<h3>The BEST Mobile Entertainment in Los Angeles</h3>
						</div>		
						<div class="os-animation" data-animation="bounceInUp" data-delay="1s">
							<h4>Its the Party that COMES to YOU!</h4>
							<a class="btn btn-outline-light btn-lg" href="#features">Learn More</a>
						</div>
					</div> <!--- End Carousel Caption -->
				</div>
				<!--Slide Three -->
				<div class="carousel-item background-3" alt="">
					<div class="dark-overlay os-animation" data-animation="slideInRight" delay=".4s"></div>
					<div class="carousel-caption text-center">
						<div class="os-animation" data-animation="bounceInUp" data-delay=".6s">
							<img class="carousel-caption-logo" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/TR_Logo/los-angeles-foam-party-totally-rad-logo-300x211.png" alt="Foam Parties">
						</div>
						<div class="os-animation" data-animation="bounceInUp" data-delay=".8s">
							<h3>YES we do FOAM PARTIES!</h3>
						</div>		
						<div class="os-animation" data-animation="bounceInUp" data-delay="1s">
							<a class="btn btn-outline-light btn-lg" href="#foam-party">Learn More</a>
						</div>
					</div> <!--- End Carousel Caption -->
				</div>
			</div> <!--- End Carousel Inner -->
			<!--- Previous & Next Buttons -->
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
		<!--- End Carousel Image Slider -->

	</div>
	<!-- End Home -->
	<?php } else if ( is_404() ) { ?>
	<!-- Start Home -->
	<div id="home">
		<div class="landing non-landing">
			<div class="home-wrap">
				<div class="home-inner OtherLandPageImage">
				</div>
				<div class="home-inner dark-overlay os-animation" data-animation="slideInLeft" delay=".4s"></div>
			</div>
		</div>

		<!-- Start Landing Page Caption -->
		<div class="caption text-center">
			<div class="os-animation" data-animation="slideInLeft" data-delay=".6s">
				<h1>WE HAVE FOUND NOTHING!</h1>
			</div>
		</div>

	</div>
	<?php 
	//other then show Blog entry point - NEWS
	} else if (is_home()) { ?>
			<!-- Start Home -->
	<div id="home">
		<div class="landing non-landing">
			<div class="home-wrap">
				<div class="home-inner OtherLandPageImage">
				</div>
				<div class="home-inner dark-overlay os-animation" data-animation="slideInLeft" delay=".4s"></div>
			</div>
		</div>

		<!-- Start Landing Page Caption -->
		<div class="caption text-center shine-logo">
			<div class="os-animation" data-animation="bounceInUp" data-delay=".6s">
				<!--<h1>Ghostly <span class="add-color-main">Games</span> </br> Entertainment</h1>-->
				<img class="front-logo shine-logo-img" src="<?php echo esc_url(get_template_directory_uri()); ?>img/TR_Logo/totallyrad_logo-FINAL.png" alt="Ghostly Games Entertainment"></img>
			</div>
			<div class="os-animation" data-animation="slideInLeft" data-delay=".6s">
				<h1>News</h1>
			</div>
		</div>

	</div>
	<?php } else {?>
	<!-- Start Home -->
	<div id="home">
		<div class="landing non-landing">
			<div class="home-wrap">
				<div class="home-inner OtherLandPageImage">
				</div>
				<div class="home-inner dark-overlay os-animation" data-animation="slideInLeft" delay=".4s"></div>
			</div>
		</div>

		<!-- Start Landing Page Caption -->
		<div class="caption text-center shine-logo">
			<div class="os-animation" data-animation="bounceInUp" data-delay=".6s">
				<!--<h1>Ghostly <span class="add-color-main">Games</span> </br> Entertainment</h1>-->
				<img class="front-logo shine-logo-img" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/TR_Logo/totallyrad_logo-FINAL.png" alt="Ghostly Games Entertainment"></img>
			</div>
			<div class="os-animation" data-animation="slideInLeft" data-delay=".6s">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>

	</div>
	<?php } ?>