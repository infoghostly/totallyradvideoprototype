<?php if( has_children() OR $post->post_parent > 0) { ?>
<nav class="site-nav children-links mobile-off">
	<span class="parent-link">Related Links</a></span>
	<ul>
		<?php 
		//Below are the arguments to set up the child listing
			$args = array('child_of' => get_top_ancestor_id() ,
		 		'title_li' => '',
		 		'depth' => 1,
		 		'link-before' => 'dropdown-item'
		 ); ?>
		<?php wp_list_pages($args); ?>
	</ul>
</nav>
<?php } ?>