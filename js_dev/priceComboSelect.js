//priceComboSelect.js
var catSel;
var servSelTime;
var servSel;
var checkOnce = true;
//Object to perform key value pairs
var whatIsChecked = {};
var priceListCont = {};
//Set up globals to store current pricing
var mainPrice = 0;
var discountPrice = 0;
var savingDollars = 0;
var booklyNextButton;
var booklyNextButtonMobile;
var booklyStatusCounter = 0;
var reloadbutton;
var thaQuotes = '"';

//a pricing object blueprint for each price/combo price
function ServicePiceInfo(serName,theTime,thePrice) {
	this.name = serName;
	this.time = theTime;
	this.price = thePrice;
	this.gettheprice = function () {
		return this.price;
	}
};

//Array.from support for IE
if (!Array.from) {
  Array.from = (function () {
    var toStr = Object.prototype.toString;
    var isCallable = function (fn) {
      return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
    };
    var toInteger = function (value) {
      var number = Number(value);
      if (isNaN(number)) { return 0; }
      if (number === 0 || !isFinite(number)) { return number; }
      return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    };
    var maxSafeInteger = Math.pow(2, 53) - 1;
    var toLength = function (value) {
      var len = toInteger(value);
      return Math.min(Math.max(len, 0), maxSafeInteger);
    };

    // The length property of the from method is 1.
    return function from(arrayLike/*, mapFn, thisArg */) {
      // 1. Let C be the this value.
      var C = this;

      // 2. Let items be ToObject(arrayLike).
      var items = Object(arrayLike);

      // 3. ReturnIfAbrupt(items).
      if (arrayLike == null) {
        throw new TypeError("Array.from requires an array-like object - not null or undefined");
      }

      // 4. If mapfn is undefined, then let mapping be false.
      var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
      var T;
      if (typeof mapFn !== 'undefined') {
        // 5. else
        // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
        if (!isCallable(mapFn)) {
          throw new TypeError('Array.from: when provided, the second argument must be a function');
        }

        // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.
        if (arguments.length > 2) {
          T = arguments[2];
        }
      }

      // 10. Let lenValue be Get(items, "length").
      // 11. Let len be ToLength(lenValue).
      var len = toLength(items.length);

      // 13. If IsConstructor(C) is true, then
      // 13. a. Let A be the result of calling the [[Construct]] internal method of C with an argument list containing the single item len.
      // 14. a. Else, Let A be ArrayCreate(len).
      var A = isCallable(C) ? Object(new C(len)) : new Array(len);

      // 16. Let k be 0.
      var k = 0;
      // 17. Repeat, while k < len… (also steps a - h)
      var kValue;
      while (k < len) {
        kValue = items[k];
        if (mapFn) {
          A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
        } else {
          A[k] = kValue;
        }
        k += 1;
      }
      // 18. Let putStatus be Put(A, "length", len, true).
      A.length = len;
      // 20. Return A.
      return A;
    };
  }());
}

$(document).ready(function() {
	var checkboxes = document.querySelectorAll('input[type=checkbox]'),
    checkboxArray = Array.from( checkboxes );

    //Set up names in the What is checked and then set up event listeners for check boxes
	checkboxArray.forEach(function(checkbox) {
		whatIsChecked[$(checkbox).attr('name')] = 0;
	  	checkbox.addEventListener('change', confirmCheck);
	});
	//Then get the time, chuck it in the whatIsCheck obj to
	var timebox = $('.price-combo-time-sel');
	whatIsChecked['time'] = $('.price-combo-time-sel').val();
	//initiate the change to update time
	timebox.on('change', changeTime);
	//Dont Display Bookley at the moment:
	$('.bookly-form').css("display","none");
	//Itterates through the serivce options - to get name hour and price
	reloadbutton = $('.reloadpage');
	reloadbutton.click(function(){location.reload(false);});
});

function updateBooklySelection(categorySel,serviceSel,fullword) {
		if(!serviceSel){
			$('.bookly-js-select-category').val(categorySel).prop('selected',true).trigger('change');
		} //trigger a change instead of click
   	    else{
   	    	$('.bookly-js-select-service').val(serviceSel).prop('selected',true).trigger('change');
   	    	////console.log("this is the price - " + priceListCont[fullword].gettheprice());

   	    	//Confirm that the selected service match - otherwise ERROR!!!
   	    	var str = $('.bookly-js-chain-item .bookly-form-group .bookly-js-select-service option:selected').text();
   	    	var res = str.substring(0,fullword.length);
   	    	if (fullword === res) {
   	    	//Now we calculate the pricing
   	    		calculateDiscountComboPricing(fullword);
   	    	} else {
   	    		alert("There is a problem - Please refresh your page. If problem persist please call Ghostly Games directly on 0406 763 101.");
   	    	}
   	    } 
	    //return false;
}

function booklyStatusChange(){
	booklyStatusCounter++;
	console.log("You have made it to bookly Button, Status number  " +  booklyStatusCounter);
}

function updateBooklySelectionReset(){
		$('.bookly-js-select-category').val("").prop('selected',true).trigger('change'); //trigger a change instead of click
   	    $('.bookly-js-select-service').val("").prop('selected',true).trigger('change'); //trigger a change instead of click
	    //return false;
}

function confirmCheck() {
	/*When user clicks on first check box, we hope that the bookly services
	options are now visable to create our pricing objects.
	We only need to do this once however. Then the program can use the stored objects for
	later use*/
	if (checkOnce) {
		//$('.bookly-js-draft .ab-formGroup .ab-select-service option').each(function(){
		$('.bookly-js-chain-item .bookly-form-group .bookly-js-select-service option').each(function(){
		    var splitedVars = this.text.split(',');
		    //trim the space
		    for (i = 0; i < splitedVars.length; i++){
		    	splitedVars[i] = splitedVars[i].trim();
		    }
		    if (splitedVars[0] != 'Select service') {
		    //new price object into array
				priceListCont[splitedVars[0] + ", " + splitedVars[1]] = new ServicePiceInfo(splitedVars[0],splitedVars[1],splitedVars[2]);
			}
		});
		//Reveal Bookly
		$('.bookly-box.bookly-bold').css("display","none")
		if(window.innerWidth >= 650 ) {
			$('.bookly-js-chain-item.bookly-table.bookly-box').css("display","none");
		}

		$('.bookly-form').css("display","block");
		booklyNextButtonMobile = $('.bookly-right.bookly-mobile-next-step.bookly-js-mobile-next-step.bookly-btn.bookly-none.ladda-button');
		booklyNextButtonMobile.click(function(){
			//location.reload(false);
			$('.combination-pricing-contatiner').css("display","none");
			$('.reloadpage').css("display","block");
			//alert('Pressed!');
		});
		booklyNextButton = $('.bookly-next-step.bookly-js-next-step.bookly-btn.ladda-button');
		booklyNextButton.click(function(){
			//location.reload(false);
			$('.combination-pricing-contatiner').css("display","none");
			$('.reloadpage').css("display","block");
			//alert('Pressed!');
		});

		//Dont do it any more
		checkOnce = false;
	}

  //Perform check process
  if (this.checked) {
  	//categorySelCont = categorySelCont + parseInt($(this).val());
  	if ($(this).attr('name') == 'VR') {
	  	if(!$('input[name="MGT"]').prop('checked')){
	  		$('input[name="MGT"]').prop('checked', true);
	  		whatIsChecked["MGT"] = 1;
	  	}
	}
	//We only want the MGTLTSP selected - nothing else
	if ($(this).attr('name') == 'MGTLTSP') {
	  	for (var key in whatIsChecked){
	  		if(!$('input[name=" '+ key +' "]').prop('checked') && key != 'MGTLTSP'){
	  			$('input[name="'+ key +'"]').prop('checked', false);
	  			whatIsChecked[key] = 0;
	  		}
		}
	} else {
		if($('input[name="MGTLTSP"]').prop('checked')){
			$('input[name="MGTLTSP"]').prop('checked', false);
		}
		whatIsChecked["MGTLTSP"] = 0;
	}
	whatIsChecked[$(this).attr('name')] = 1;
	startProcess(whatIsChecked);
    //alert('checked !! ' + categorySelCont);
  } else {
  	//categorySelCont = categorySelCont - parseInt($(this).val());
  	if ($(this).attr('name') == 'MGT') {
	  	if($('input[name="VR"]').prop('checked')){
	  		$('input[name="VR"]').prop('checked', false);
	  		whatIsChecked["VR"] = 0;
	  	}
	}
	whatIsChecked[$(this).attr('name')] = 0;
	startProcess(whatIsChecked);
  	//alert('not checked!! ' + categorySelCont)
  }
}

function startProcess(w){
	if (confirmIfCheckedAtAll(w)) {
		checkIfComboSelectedDisable1hour(w);
		setCatSerValues(generateWord(w),w["time"]);
	}
}

function changeTime(){
	//check lock
	var cl = $('.price-combo-time-sel').attr('lock-att');
	if (typeof cl !== typeof undefined || cl !== false) {
		if (cl != 'locked'){
			//Update the time in whatIsCheck obj
			whatIsChecked['time'] = $('.price-combo-time-sel').val();
			//startProcess(whatIsChecked);
			if (confirmIfCheckedAtAll(whatIsChecked)) {
				setCatSerValues(generateWord(whatIsChecked),whatIsChecked["time"]);
			}
		}
	} else {
		//Update the time in whatIsCheck obj
		whatIsChecked['time'] = $('.price-combo-time-sel').val();
		//startProcess(whatIsChecked);
		if (confirmIfCheckedAtAll(whatIsChecked)) {
			setCatSerValues(generateWord(whatIsChecked),whatIsChecked["time"]);
		}
	}
}

function confirmIfCheckedAtAll(w){
	var countSel = 0;
	for (var key in w){
		if ( key == "time"){
			continue; 
		}
		countSel = countSel + parseInt(w[key]);
	}
	if (countSel < 1) {
		//Reset all values
		updateBooklySelectionReset();
		catSel = 0;
		servSel = 0;
		mainPrice = 0;
		discountPrice = 0;
		savingDollars = 0;
		dicounteddisplay();
		return false;
	} else {
		return true;
	}
}

function checkIfComboSelectedDisable1hour(w){
	var countSel = 0;
	for (var key in w){
		if ( key == "time" || key == 'VR' ){
			continue; 
		}
		countSel = countSel + parseInt(w[key]);
	}

	var checkMGTLTSPcount = parseInt(w["MGTLTSP"]);

	if (countSel > 1 || checkMGTLTSPcount == 1) {
		$('.price-combo-time-sel').find('option:nth-child(1)').attr('disabled',true);
		//Lock the time selector - by setting att
		$('.price-combo-time-sel').attr('lock-att','locked');
		$('.price-combo-time-sel').find('option:nth-child(2)').attr('selected',true).trigger('change');
		//Remove lock
		$('.price-combo-time-sel').removeAttr('lock-att');
		$('.price-combo-time-sel').find('option:nth-child(1)').attr('selected',false).trigger('change');
	} else {
		$('.price-combo-time-sel').find('option:nth-child(1)').attr('disabled',false);
	}
}

function generateWord(w){
	var word = "";
	for (var key in w){
		if ( key == "time"){
			continue; 
		}
		if (w[key] === 1) {
			if (word.length < 1) {
				word = key;
			} else {
				word = word + " + " + key;
			}
		}
	}
	return word;
}

function setCatSerValues(word,time){
	var fullword = word + ", " + time + " h";
	var checkword;
	if(!word.length < 1){
		//Select Category
		$('.bookly-js-select-category option:contains(' + word + ')').filter(function() {
			if ($(this).text() === word){
				checkword = $(this).val();
		    }
		});
	}
	//Stop the service loop
	if( catSel != checkword || servSelTime != time ){
		catSel = checkword;
		servSelTime = time;
		if (parseInt(catSel) > 0){
			//console.log("the catSel " + catSel + " and the serv value " + servSel);
			updateBooklySelection(catSel,false,0);
			//Find the Select Service now
			$('.bookly-js-select-service option:contains(' + fullword + ')').filter(function() {
				if ($(this).text().substring(0,fullword.length) === fullword){
					servSel = $(this).val();
			    }
			});
			updateBooklySelection(false,servSel,fullword);
		}
	}
}

function calculateDiscountComboPricing (fullword){
	var displayHtml = "";
	//Split the time and cats
	var splitedVars = fullword.split(',');
	//trim the space
	for (i = 0; i < splitedVars.length; i++){
		splitedVars[i] = splitedVars[i].trim();
	}
	//Split the cats
	var splitedVarsCats = splitedVars[0].split('+');
	if (splitedVarsCats.length > 1) {
		var calcMainPrice = 0;
		for (i = 0; i < splitedVarsCats.length; i++){
			splitedVarsCats[i] = splitedVarsCats[i].trim();
			if (splitedVarsCats[i] !== 'VR') {
				//console.log("Getting the price for " + [splitedVarsCats[i] + ", " + splitedVars[1]] + ".");
				calcMainPrice = calcMainPrice + parseInt(priceListCont[splitedVarsCats[i] + ", " + splitedVars[1]].gettheprice());
				var theIndprice = priceListCont[splitedVarsCats[i] + ", " + splitedVars[1]].gettheprice();
				displayHtml += "<p> Service " + splitedVarsCats[i] + " priced at $" + theIndprice + "</p>";
			} else {
				//Confirm if combo or not - else skip
				if(splitedVarsCats[i + 1] != undefined){
					//console.log("Since this VR is in combo + 70 from the main price to cal savings")
					calcMainPrice = calcMainPrice + 70;
					displayHtml += "<p> Service " + splitedVarsCats[i] + " priced at $70</p>";
				} else {
					displayHtml += "<p> Service " + splitedVarsCats[i] + " priced at $70</p>";
				}
			}
		}
		mainPrice = calcMainPrice;
		discountPrice = priceListCont[fullword].gettheprice();
		savingDollars = mainPrice - discountPrice;
		//This basically happens if its just MGT + VR only
		if (savingDollars < 0) {
			mainPrice = discountPrice;
		}
	} else {
		discountPrice = -1;
		savingDollars = 0;
		var theIndprice = priceListCont[splitedVarsCats[0] + ", " + splitedVars[1]].gettheprice();
		displayHtml += "<p> Service " + splitedVarsCats[0] + " priced at $" + theIndprice +  "</p>";
		mainPrice = priceListCont[fullword].gettheprice();
	}
	if (splitedVars[1].substring(0,1) == 1) {
		displayHtml += "<p> Selected time of " + splitedVars[1] + "our</p>";
	} else {
		displayHtml += "<p> Selected time of " + splitedVars[1] + "ours</p>";
		displayHtml += "<p> <strong>SPECIAL PRICE 2 hours +</strong></p>";
	}
	if (discountPrice < 1 || mainPrice <= discountPrice) {
		//console.log("main Price $" + mainPrice);
		displayHtml += "<p><hr /></p>";
		displayHtml += "<p><strong style=" + thaQuotes + "font-size: 20px;" + thaQuotes + ">Total Price is $" + mainPrice + "</strong></p>";
		nondiscountdisplay(fullword,displayHtml);
	} else {
		//console.log("main Price $" + mainPrice + " discount Price $" + discountPrice + " Saving dollars $" + savingDollars);
		displayHtml += "<p><hr /></p>";
		displayHtml += "<p>Total Price was $" + mainPrice + "</p>";
		displayHtml += "<p><strong class=" + thaQuotes + "discounttxt" + thaQuotes + " style=" + thaQuotes + "font-size: 20px;" + thaQuotes + ">Total Price is now $" + discountPrice + "</strong></p>";
		displayHtml += "<p class=" + thaQuotes + "savingstxt" + thaQuotes + " style=" + thaQuotes + "color: #f50707;font-size: 20px;" + thaQuotes + ">YOU SAVED $" + savingDollars + "!!</p>";
		dicounteddisplay(fullword,displayHtml);
	}
	displayServiceAndImages(fullword);
}

function nondiscountdisplay(fw,displayHtml){
	$('.rib-price-text').text("Your Price is $" + mainPrice);
	$('.combination-main-price').html("<h3>Service(s) selected:</h3>" + displayHtml);
	$('.combination-main-features').html(eval(createhtmlinformationpan(fw)));
}

function dicounteddisplay(fw,displayHtml){
	if(mainPrice < 1){
		$('.combination-main-features').text("");
		$('.combination-main-price').text("There is no Service Selected.")
		$('.rib-price-text').text("No Service Selected");
		clearGhostImages();
	} else {
		$('.rib-price-text').text("Combo Price!! $" + discountPrice);
		$('.combination-main-price').html("<h3>COMBO Services selected:</h3>" + displayHtml);
		$('.combination-main-features').html(eval(createhtmlinformationpan(fw)));
	}
}

function createhtmlinformationpan(fw){
	var htmlreturn = "";
	var splitedVarsCats = passOnlyCategories(fw);
	if (splitedVarsCats.length > 1) {
		//Add the capacity information here
		var checkVRcat = splitedVarsCats[1].trim();
		if(checkVRcat == "VR"){
			if (splitedVarsCats.length > 2) {
				htmlreturn += "$('.service-information-pannel .combo-info-cap').html() + ";
			}
		} else {
			htmlreturn += "$('.service-information-pannel .combo-info-cap').html() + ";
		}
		for (i = 0; i < splitedVarsCats.length; i++){
			splitedVarsCats[i] = splitedVarsCats[i].trim();
			htmlreturn += "$('.service-information-pannel ." + splitedVarsCats[i] + "-info').html()";
			if (i + 1 < splitedVarsCats.length){
				htmlreturn += " + "
			}
			//console.log(htmlreturn);
		}
		//console.log("the return " + htmlreturn);
		return htmlreturn;
	} else {
		if (splitedVarsCats[0] == "MGTLTSP") {
			htmlreturn += "$('.service-information-pannel .MGTLTSP-info-cap').html() + ";
		}
		//console.log("$('.service-information-pannel ." + splitedVarsCats[0] + "-info').html()");
		htmlreturn += "$('.service-information-pannel ." + splitedVarsCats[0] + "-info').html()";
		return htmlreturn;
	}
}


function displayServiceAndImages(fw){
	clearGhostImages();
	var ghostCount = 0;
	var splitedVarsCats = passOnlyCategories(fw);
	if (splitedVarsCats.length > 1) {
		for (i = 0; i < splitedVarsCats.length; i++){
			splitedVarsCats[i] = splitedVarsCats[i].trim();
			ghostCount++;
			if (splitedVarsCats[i] == 'VR') {
				ghostCount--;
				$('.ghost-img-div-1').html($('.serivce-ghosts-images-hidden .MGT-VR-ghost').html()); 
			} else {
				if (ghostCount == 1) {
					htmlreturn = "$('.serivce-ghosts-images-hidden ." + splitedVarsCats[i] + "-ghost').html()";
					$('.ghost-img-div-1').html(eval(htmlreturn));
				}
				if (ghostCount == 2) {
					htmlreturn = "$('.serivce-ghosts-images-hidden ." + splitedVarsCats[i] + "-ghost').html()";
					$('.ghost-img-div-2').html(eval(htmlreturn));
				}
				if (ghostCount >= 3 ) {
					htmlreturn = "$('.serivce-ghosts-images-hidden ." + splitedVarsCats[i] + "-ghost').html()";
					$('.ghost-img-div-3').html(eval(htmlreturn));
				}
			}
		//console.log("the return " + htmlreturn);
		}
	} else {
		//console.log("$('.service-information-pannel ." + splitedVarsCats[0] + "-info').html()");
		htmlreturn = "$('.serivce-ghosts-images-hidden ." + splitedVarsCats[0] + "-ghost').html()";
		$('.ghost-img-div-1').html(eval(htmlreturn));
	}
	$('.rib-service-text').text(passbackJustOneWord(fw));
}

function clearGhostImages(){
	$('.ghost-img-div-1').html("");
	$('.ghost-img-div-2').html("");
	$('.ghost-img-div-3').html("");
	$('.rib-service-text').text('');
}

function passbackJustOneWord(fw){
	//Split the time and cats
	var splitedVars = fw.split(',');
	return splitedVars[0];
}

function passOnlyCategories(fw){
	//Split the time and cats
	var splitedVars = fw.split(',');
	//trim the space
	for (i = 0; i < splitedVars.length; i++){
		splitedVars[i] = splitedVars[i].trim();
	}
	//Split the cats
	var splitedVarsCats = splitedVars[0].split('+');
	return splitedVarsCats;
}
//To set on display with the services:
//$('.combination-main-features').html($('.service-information-pannel .MGT-info').html() + $('.service-information-pannel .VR-info').html())
