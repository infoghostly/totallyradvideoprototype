import $ from '../js/jquery-3.3.1.min.js';
import NavMenu from './modules/NavMenu';
import SmoothScrollOnATag from './modules/SmoothScrollOnATag';
import BouncingDownArrowDisapear from './modules/BouncingDownArrowDisapear';
import PricingToggleInfo from './modules/PricingToggleInfo';
//import MobileMenuHambergurAnimation from './modules/MobileMenuHambergurAnimation';

$(document).ready(function() {
	//Bootstrap add for the nav menu
	new NavMenu();
	new SmoothScrollOnATag();
	new BouncingDownArrowDisapear();
	new PricingToggleInfo($('.card-dropdown-toggle-MGT'));
	new PricingToggleInfo($('.card-dropdown-toggle-LT'));
	new PricingToggleInfo($('.card-dropdown-toggle-VR'));
	new PricingToggleInfo($('.card-dropdown-toggle-MGTSP'));
	new PricingToggleInfo($('.card-dropdown-toggle-DJ'));
});


