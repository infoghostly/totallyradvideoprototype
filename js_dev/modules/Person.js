class Person {
	constructor( fullName, favColor){
		this.name = fullName;
		this.favoriteColor = favColor;
	}


	greet() {
		console.log("Hi there this is, " + this.name + " Fav colour is " + this.favoriteColor + ".");
	}
}

//Node JS way to export a JS class
//module.exports = Person;

//ES6 way to export a class
export default Person;