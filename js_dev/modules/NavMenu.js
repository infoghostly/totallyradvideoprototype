import $ from '../../js/jquery-3.3.1.min.js';
class NavMenu {
	constructor(){
		this.getNavBarNav = $('#menu-primary-menu-links')
		this.getMenuItems = $('#menu-primary-menu-links li');
		this.menuIcon = $(".site-header__menu-icon");
		this.windowItem = $(window);
		this.menuIconToggeler = $('.navbar-toggler');
		this.documentItem = $(document);
		this.navBarItem = $('.navbar');
		this.addNavItem();
		this.mobileIconEvents();
		this.navBarTranToSolid();
		//this.closeNavMobileOnClick();
		this.removeClassOnNavBar();
		this.toggleDark()
	}

	/*This is to add the nessesary classes for the BootStrap system to work
	on WP nav menu systems*/
	addNavItem(){
		this.getMenuItems.each(function( index ){
			$(this).addClass('nav-item');
			if ($(this).hasClass('menu-item-has-children')){
				$(this).addClass('dropdown');
				$(this).children().each(function(){
					if($(this).is("a")){
						$(this).addClass('nav-link dropdown-toggle');
						$(this).attr("role","button");
						$(this).attr("data-toggle","dropdown");
						$(this).attr("aria-haspopup","true");
						$(this).attr("aria-expanded","false");
					}
					if($(this).is("ul")){
						$(this).addClass('dropdown-menu animate slideIn');
						$(this).children().children().each(function(){
								if($(this).is("a")){
								$(this).addClass('dropdown-item');
							}
						});
					}
				});
			} else {
				$(this).children().each(function(){
					if($(this).is("a") && !$(this).parent().parent().parent().hasClass('menu-item-has-children')){
						$(this).addClass('nav-link');
						return false;
					}
				});
			}
		});
	}

	/******* icon animation toggle BEGIN *******/
	mobileIconEvents() {
		this.menuIcon.click(this.toggleTheMenu.bind(this));
	}

	toggleTheMenu(){
		//Perform Menu icon animation with the x close button
		this.menuIcon.toggleClass("site-header__menu-icon--close-x");
		this.toggleDark();
	}
	/******* icon animation toggle - END *******/

	/*==================NAVBAR TRANSPARENT TO SOLID======================*/
	navBarTranToSolid(){
		let that = this;
		this.windowItem.scroll(function ( ) {
			if($(this).scrollTop() > 300){
				that.navBarItem.addClass('solid');
			} else {
				that.navBarItem.removeClass('solid');
			}
		});
	}

	/*==================Close Mobile Nav on click======================*/
	closeNavMobileOnClick() {
		let that = this;
		this.documentItem.click(function ( event ){
			let clickover = $(event.target);
			let _opened = $(".navbar-collapse").hasClass("show");
			if( _opened === true && !clickover.hasClass("navbar-toggler")){
				$(".navbar-toggler").click();
			}
		});
	}

	/*Everything should be ready now, show the nav bar*/
	removeClassOnNavBar(){
		this.getNavBarNav.css('opacity','1');
	}

	/*===========Toggle the dark nav bar when at the top ===========*/
	toggleDark() {
		let that = this;
		this.menuIconToggeler.click(function ( event ){

			let loc = $(window).scrollTop();
			if (loc < 300 && $(this).hasClass('site-header__menu-icon--close-x')){
				that.navBarItem.addClass('solid');
			} else if (loc < 300 && that.navBarItem.hasClass('solid')){
				that.navBarItem.removeClass('solid');
			}
		});
	}

}


export default NavMenu;