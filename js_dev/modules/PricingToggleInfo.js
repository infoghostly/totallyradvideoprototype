import $ from '../../js/jquery-3.3.1.min.js';
class PricingToggleInfo {
	constructor(toggler){
		this.dropdownTogger = toggler;
		this.events();
	}

	//2. Events to trigger on
	events() {
		this.dropdownTogger.on('click',(function(){
		  		$(this).next('.card-dropdown').slideToggle("fast");
			})
		);
	}


}

export default PricingToggleInfo;