import $ from '../../js/jquery-3.3.1.min.js';		
class SmoothScrollOnATag {
	/*
	Rules:
		* Mark an element with an "id" of a desired name you wish to scroll to
		* Create an ancour tag on the page - ensure you have #<destination> on the
		  HREF Value
	*/
	constructor(){
		this.aElements = $("a");
		this.checkHash();
	}

	/*===========This will enable smoth scrolling on a page==========*/
	checkHash(){
		this.aElements.on('click' , function(event){
			if (this.hash !== "") {
				let hash = this.hash;
				$('html, body').animate ( {
					scrollTop: $(hash).offset().top
				}, 800, function() {
					window.location.hash = hash;
				});
			}
		});
	}

}

export default SmoothScrollOnATag;