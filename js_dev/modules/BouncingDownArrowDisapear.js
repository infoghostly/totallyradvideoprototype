import $ from '../../js/jquery-3.3.1.min.js';		
class BouncingDownArrowDisapear {
	constructor(){
		this.windowItem = $(window);
		this.bounceArrow = $(".arrow");
		this.pinkPhone = $(".floating-phone a img.phone-icon");
		this.arrowDisapearCheck();
		this.phoneDisapearCheck();
	}

	/*===========Remove the bouncing arrow at 200px==========*/
	arrowDisapearCheck(){
		let that = this;
		this.windowItem.scroll(function ( ) {
			that.bounceArrow.css("opacity", 1 - $(this).scrollTop() / 250);
		});
	}

	/*===========Remove the bouncing arrow at 200px==========*/
	phoneDisapearCheck(){
		let that = this;
		this.windowItem.scroll(function ( ) {
			if($(this).scrollTop() > 600){
				that.pinkPhone.addClass('active');
			} else {
				that.pinkPhone.removeClass('active');
			}
		});	
	}

}

export default BouncingDownArrowDisapear;