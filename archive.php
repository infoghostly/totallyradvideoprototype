<?php

get_header();

if(have_posts()) : ?>
	
	<h2> <?php 
	if( is_category() ) {
		single_cat_title();
	} elseif ( is_tag() ) {
		single_tag_title();
	} elseif ( is_author() ){
		the_post();
		echo 'The Author Space: ' . get_the_author();
		//The rewind posts is to ensure we do not effect the loop - meaning miss the first post -- test 1234
		rewind_posts();
	} elseif ( is_day() ) {
		echo 'Daily Archives: ' . get_the_date();
	} elseif ( is_month() ){
		echo 'Montly Archives: ' . get_the_date('F Y');
	} elseif ( is_year() ) {
		echo 'Yearly Archives: ' . get_the_date('Y');
	} else { echo 'Archives';}

	?></h2>

	<?php while (have_posts()) : the_post(); 
		get_template_part('content', get_post_format());
	endwhile;

	get_template_part('paginatelinks');
else :
	echo '<p>No content found </p>';
endif;

get_footer();
?>