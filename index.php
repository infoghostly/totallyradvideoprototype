<?php

get_header(); ?>
	<!-- Start main-colum Div this is a test to see MIKE 2 -->
	<?php //get_sidebar(); ?>
	<div class="main-colum container-fluid">
		<?php if(have_posts()) :
		while (have_posts()) : the_post(); 
			
			get_template_part('content', get_post_format());

		endwhile;


		get_template_part('paginatelinks');

		else :
			echo '<p>No content found </p>';
		endif; ?>
	</div>
	<!-- End main-colum Div -->

<?php get_footer();
?>