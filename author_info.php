<!-- Start author-info-container Div -->
<div class="container-fluid author-info-container">
	<div class="narrow">
	<div class="row">
	<!-- Start author-img-col Div -->
	<div class="author-img-col col-md-3">
		<!-- Start auth-img-container Div -->
		<div class="author-img-container">
			<?php echo get_avatar(get_the_author_meta('ID',$authID), 512); ?>
			<p><?php echo get_the_author_meta('nickname',$authID); ?> </p>
		</div>
		<!-- End auth-img-container Div -->
		<p>Email the author: <a href="mailto:<?php echo get_the_author_meta('email',$authID); ?>"><?php echo get_the_author_meta('display_name',$authID);; ?></a></p>
	</div>
	<!-- End author-img-col Div -->
	<?php $otherAuthorPosts = new WP_Query(array(
		'author' => $authID,
		'posts_per_page' => 3,
		'post__not_in' => array(get_the_ID())
	)); ?>
	<!-- Start author-content-col Div -->
	<div class="author-content-col col-md-9">
		<h3>Information About <?php echo get_the_author_meta('nickname',$authID); ?> </h3>
		<?php echo wpautop(get_the_author_meta('description',$authID)); ?>
		
		<?php //Check to see if there are other posts by the author
		if ($otherAuthorPosts->have_posts()) { ?>
		<!-- Start other-posts-by Div -->	
		<div class="other-posts-by">
			<h4>Other posts by <?php echo get_the_author_meta('nickname',$authID); ?></h4>
			<ul>
				<?php 
					while ($otherAuthorPosts->have_posts()){
						$otherAuthorPosts->the_post();
					?>
					<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
			<?php }?>

			</ul>
		</div>
		<!-- End other-posts-by Div -->
		<?php } ?>
		<?php //Count how many posts the current author has, if more than required, display btn
		if (count_user_posts(get_the_author_meta('ID',$authID)) > 3) { ?>
		<!-- Start button-container Div -->
		<div class="author-button-container">
			<a class="btn btn-custcolor btn-sm" href="<?php echo get_author_posts_url(get_the_author_meta('ID',$authID)); ?>">View all posts by <?php echo get_the_author_meta('nickname',$authID); ?></a>
		</div>
		<!-- End button-container Div -->
		<?php } wp_reset_postdata();?>
	</div>
	<!-- End author-content-col Div -->
	</div><!-- End of Row -->
	</div><!-- End of Row and Narrow -->
</div>
<!-- End author-info-container Div -->