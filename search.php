<?php

get_header();

if(have_posts()) :	?>
		
	<!--Search Results page-->
	<h3 class="search-h3">Search Results for: <?php the_search_query(); ?></h3>
	<?php while (have_posts()) : the_post();
		get_template_part('content', get_post_format());
	endwhile;

	get_template_part('paginatelinks');

else :
	echo '<p>No content found </p>';
endif;

get_footer();
?>