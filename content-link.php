<article class="post post-link-container">
	<!-- Start post-link Div -->
	<div class="post-link">
		<a href="<?php echo get_the_content(); ?>"><?php the_title(); ?></a>	
	</div>
	<!-- End post-link Div -->
</article>