<?php

get_header(); ?>



<!-- Start features -->
<div id="features" class="offset">	

	<!-- Start Animation -->
	<div class="os-animation" data-animation="fadeInUp">
		<div class="narrow text-center">
			<div class="col-12">
				<h1 class="heading">Totally Rad Game Truck, Mobile Laser Tag and Foam Parties</h1>
				<h2 class='heading sm-dark'>VIRTUAL REALITY GAMING, RACING SIMULATOR, FORTNITE, OCULUS, MOBILE LASER TAG AND FOAM PARTIES. BIRTHDAY – SCHOOL FUNCTIONS – CORPORATE EVENTS</h2>
				<p class="lead"><strong>A Video Game Truck unlike all the others…</strong><br>There are a lot of video game trucks in Los Angeles.  They all offer video games in a trailer, van or bus that comes to birthday parties, corporate events, promotions and the like…but that’s where the similarities end.</p>
				<a class="btn btn-secondary btn-sm" href="#contact">Request a Quote</a>
				<a class="btn btn-custcolor btn-sm" href="#portfolio">See Our Portfolio</a>
			</div>
		</div>
	</div>
	<!-- End Animation -->

	<!--Start Jumbotron -->
	<div class="jumbotron">
		<div class="narrow">
			
			<div class="os-animation" data-animation="fadeInUp">
				<h3 class="heading">So, what do we have?</h3>
				<div class="heading-underline"></div>
			</div>

			<div class="row">
				<div class="col-sm-6 col-md-4">
					<div class="os-animation" data-animation="fadeInLeft">
						<div class="feature">
							<span class="fa-layers cus-sm-feat-img">					
								<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/icons/red-game-controler.svg" alt="Video Game Party"></img>
							</span>
							<h3>Mobile Game Theatre</h3>
							<p>Totally Rad Video Games doesn’t just match the competition…we <strong>blow it away!</strong> We’ve got 15 gaming consoles from XBox One, PlayStation 4 and Nintendo Switch.</p>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-4">
					<div class="os-animation" data-animation="fadeInUp">
						<div class="feature">
							<span class="fa-layers fa-4x">
								<i class="fa fa-circle"></i>
								<i class="fas fa-desktop fa-inverse" data-fa-transform="shrink-8 left-1"></i>
							</span>
							<h3>4K High Definition</h3>
							<p>With 8 awesome 55 inch 4K TV's, games look way cooler and room to have games that have 8 players on them! No fuss!</p>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-4">
					<div class="os-animation" data-animation="fadeInRight">
						<div class="feature">
							<span class="fa-layers cus-sm-feat-img">					
								<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/icons/red-icons-vr.svg" alt="Virtual Reality"></img>
							</span>
							<h3>Virtual Reality</h3>
							<p>We bring a PlayStation VR Racing Simulator and <strong>TWO Oculus Rift VR Stations</strong>, complete with the Oculus Rift VR Touch (Hand) Controllers!  It will blow your mind!.</p>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-4">
					<div class="os-animation" data-animation="fadeInLeft">
						<div class="feature">
							<span class="fa-layers cus-sm-feat-img">					
								<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/icons/red-icons-laser-tag.svg" alt="Laser Tag"></img>
							</span>
							<h3>Laser Tag</h3>
							<p>Totally Rad Laser Tag is awesome!  Our high-tech Laser Taggers are accurate day or night, indoors or out! With the most advanced features and realistic “battlefield” sound, we bring “combat” right to your back yard, park, school, or gymnasium!</p>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-4">
					<div class="os-animation" data-animation="fadeInUp">
						<div class="feature">
							<span class="fa-layers cus-sm-feat-img">					
								<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/icons/red-stearing-wheel.svg" alt="PS4 Virtual Reality Racing Simulator"></img>
							</span>
							<h3>PS4 VR Racing Simulator</h3>
							<p>Totally Rad Video Games puts you in the driver’s seat! Your the driver in the race…see, hear and FEEL the action! Put on the headset, grab the wheel, get on the gas pedal and race for the checkered flag!  It’s the next best thing to getting on the track, without any fear of crashing!  There’s excitement around every turn!</p>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-4">
					<div class="os-animation" data-animation="fadeInRight">
						<div class="feature">
							<span class="fa-layers fa-4x">
								<i class="fa fa-circle"></i>
								<i class="fas fa-cloud fa-inverse" data-fa-transform="shrink-7 left-2"></i>
							</span>
							<h3>Foam Parties!</h3>
							<p>Did we mention we also do Foam Parties? Have a foam party in your own backyard! Our special foam formula doesn’t harm grass or clothing. Our foam parties are guaranteed to produce foam for 90 Min of fun!</p>
						</div>
					</div>
				</div>
			</div><!--End Row-->

		</div> <!-- End narrow -->

	</div>
	<!--End Jumbotron -->
		<!--Start of the Portfolio Grid-->
	<div class="container-fluid">
		<!-- Start Animation -->
		<div class="os-animation" data-animation="fadeInUp">
			<div class="narrow text-center">
				<div class="col-12">
					<img class="" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/TR_Site_Images/party-to-you-place.png" alt="We bring in the party to your place!">
					<div class="heading-underline"></div>
					<p class="lead">Stadium Seating, Full Climate Control and our own whisper-quiet generator let us bring the gaming to you wherever you are!  We come to your driveway or curb, your school or church, office parking lot…just about anywhere!</br>Our Game Coach runs the show while you relax!  Your home stays clean while your guests party and game in a clean, safe, and FUN environment!</p>
					<a class="btn btn-secondary btn-sm" href="#contact">Request a Quote</a>
				</div>
			</div>
		</div>
	</div>
	<div class="jumbotron">
			<div class="os-animation" data-animation="fadeInUp">
				<h3 class="heading">Oculus Rift Virtual Reality!</h3>
				<div class="heading-underline"></div>
			</div>
			<div class="narrow">
				<div class="row no-padding">

					<div class="col-md-8">
						<div class="os-animation" data-animation="fadeInLeft">
						    <picture>
	      						<source srcset="<?php echo esc_url(get_template_directory_uri()); ?>/img/TR_Site_Images/only-los-angeles-video-game-truck.png" alt="Man With Trailer - Only L.A Game Truck to bring you Oculus Rift Virtual Reality" media="(min-width: 640px)">
	      						<img srcset="<?php echo esc_url(get_template_directory_uri()); ?>/img/TR_Site_Images/only-los-angeles-video-game-truck-Mobile.png" alt="Only L.A Game Truck to bring you Oculus Rift Virtual Reality" class="fn-img-align">
	            			</picture>
						</div>
					</div>

					<div class="col-md-4">
						<div class="os-animation" data-animation="fadeInRight">
							<div class="narrow text-center">
									<h3 class='heading sm-dark'>Totally Rad Video Games offers so much more!</h3>
									<p class="lead">Up to 28 can play at once! Our HUGE mobile game theater brings 32 feet of luxurious gaming space, complete with Stadium-Style seating, high ceilings, upgraded twin air conditioning units, laser and neon lighting, and powerful sound bars under each screen for an immersive experience, our audio systems surrounds the players with rich sound.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	<div class="container-fluid">
		<div class="narrow">
			<div class="row no-padding">
				<div class="col-md-9">
					<!-- Start Animation -->
					<div class="os-animation" data-animation="fadeInLeft">
						<div class="narrow text-center">
								<h3 class='heading sm-dark'>Yes, we have Fortnite!</h3>
								<p class="lead">Add Fortnite to your video game party for just $50! (Must have a high-speed connection within 300′ of our game truck. We connect by ethernet cable to your network port or router, or our high-speed wireless hotspot if remote location. Game play will depend on a quality high-speed connection.)  Add during booking!</p>
								<a class="btn btn-secondary btn-sm" href="#contact">Ask us how Today!</a>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="os-animation" data-animation="fadeInRight">
						<img class="fn-img-align" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/TR_Site_Images/fortnite-logo-272x182.png" alt="We have Fortnite - enquire nowQ">
					</div>
				</div>
				<!-- End col-md-3 Div -->
			</div>
		</div>
	</div>
	<!-- Start Fixed Background IMG Dark -->
	<div class="fixed-background">
		<div class="dark">
		<div class="row dark-row">
			<div class="col-12 os-animation" data-animation="fadeInUp">
				<h3 class="heading">We’ve got the Best Video & VR Games!</h3>
				<div class="heading-underline"></div>
				<p class="lead">With a wide range of games and consoles at your disposal, you can ensure that there will be enough fun for everyone no matter what they choose.</p>
			</div>

			<div class="col-md-3">
				<div class="os-animation" data-animation="fadeInLeft">
					<h3>XBOX ONE X</h3>
					<div class="feature">
						<span class="fa-layers fa-3x">
							<i class="fab fa-xbox"></i>
						</span>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="os-animation" data-animation="fadeInUp">
					<h3>PS4 PRO</h3>
					<div class="feature">
						<span class="fa-layers fa-3x">
							<i class="fab fa-playstation"></i>
						</span>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="os-animation" data-animation="fadeInUp">
					<h3>Nintendo SWITCH</h3>
					<div class="feature">
						<span class="fa-layers cus-sm-feat-img">					
							<img class="nintendo-icon" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/icons/red-nintendo-switch.svg" alt=""></img>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="os-animation" data-animation="fadeInRight">
					<h3>Oculus Rift VR</h3>
					<div class="feature">
						<span class="fa-layers fa-3x">
							<i class="far fa-eye"></i>
						</span>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="os-animation" data-animation="fadeInUp">
					<h2 class="heading">Want the best Video game party?</h2>
					<p class="lead">Well what are you waiting for? <a href="#contact"><strong>Contact Us</strong> </a>now!</p>
					<div class="heading-underline"></div>
				</div>
			</div>
		</div><!--END of row dark-->
		</div>
		<div class="fixed-wrap"><!--where the fixed image is located -->
			<div id="fixed">
			</div>
		</div>
	</div>
<!-- End Fixed Background IMG Dark -->
</div>
<!-- End features  -->

<!-- Start Porfolio -->
<div id="portfolio" class="offset">	

	<div class="row padding">
		<div class="col-12 os-animation" data-animation="fadeInUp">
			<h3 class="heading">Portfolio</h3>
			<div class="heading-underline"></div>
		</div>
	</div> <!--End Row-->

	<!--Start of the Portfolio Grid-->
	<div class="container-fluid">
		<div class="row no-padding">
			
			<div class="col-sm-6 col-md-3">
				<div class="os-animation" data-animation="bounceInLeft" data-delay=".2s">
					<div class="portfolio-item">
						<a href="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFullImg/portFull1.jpg" data-lightbox="example-set" data-title="Totally Rad Games Trailer - Best Video Game Party Experience Ever!">
							<img class="example-image" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFolioImg/port1.jpg" alt="Best Video Game Party Experience Ever!"></img>
						</a>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-3">
				<div class="os-animation" data-animation="bounceInLeft">
					<div class="portfolio-item">
						<a href="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFullImg/portFull2.jpg" data-lightbox="example-set" data-title="Red Gloss Interior">
							<img class="example-image" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFolioImg/port2.jpg" alt="Red Gloss Interior"></img>
						</a>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-3">
				<div class="os-animation" data-animation="bounceInRight">
					<div class="portfolio-item">
						<a href="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFullImg/portFull3.jpg" data-lightbox="example-set" data-title="Inside of the Game Theatre - Video Game Party">
							<img class="example-image" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFolioImg/port3.jpg" alt="Inside of the Game Theatre - Kids party bus"></img>
						</a>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-3">
				<div class="os-animation" data-animation="bounceInRight" data-delay=".2s">
					<div class="portfolio-item">
						<a href="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFullImg/portFull4.jpg" data-lightbox="example-set" data-title="PS4 VR Racing Simulator">
							<img class="example-image" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFolioImg/port4.jpg" alt="PS4 VR Racing Simulator"></img>
						</a>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-3">
				<div class="os-animation" data-animation="bounceInLeft" data-delay=".2s">
					<div class="portfolio-item">
						<a href="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFullImg/portFull5.jpg" data-lightbox="example-set" data-title="Inside of the Game Theatre - Video Game Party">
							<img class="example-image" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFolioImg/port5.jpg" alt="Inside of the Game Theatre - Video Game Party"></img>
						</a>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-3">
				<div class="os-animation" data-animation="bounceInLeft">
					<div class="portfolio-item">
						<a href="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFullImg/portFull6.jpg" data-lightbox="example-set" data-title="Girl Playing - Just Dance">
							<img class="example-image" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFolioImg/port6.jpg" alt="Girl Playing - Just Dance"></img>
						</a>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-3">
				<div class="os-animation" data-animation="bounceInRight">
					<div class="portfolio-item">
						<a href="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFullImg/portFull8.jpg" data-lightbox="example-set" data-title="Coordination is the key to winning a game of Laser Tag Skirmish">
							<img class="example-image" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFolioImg/port8.jpg" alt="Laser Tag Skirmish"></img>
						</a>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-3">
				<div class="os-animation" data-animation="bounceInRight" data-delay=".2s">
					<div class="portfolio-item">
						<a href="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFullImg/portFull7.jpg" data-lightbox="example-set" data-title="Coordination is the key to winning a game of Laser Tag Skirmish">
							<img class="example-image" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/PortFolioImg/port7.jpg" alt="Coordination is the key to winning a game of Laser Tag Skirmish"></img>
						</a>
					</div>
				</div>
			</div>

		</div><!--End Row-->
	</div><!--End of Container -->
	<!--End of the Portfolio Grid-->

	<div class="os-animation" data-animation="fadeInUp">
		<div class="narrow text-center">
			<div class="col-12">
				<p class="lead">If you’re planning a birthday party, Christmas party, company event, business function, bucks night, fundraiser, video game party or just about any other kind of event, Ghostly Games Entertainment will bring the kids party bus to you.</p>
				<a class="btn btn-secondary btn-sm" href="#contact">Get In Touch</a>
			</div>
		</div>
	</div>

</div>
<!-- End Porfolio  -->
<!-- Start Foam Party -->
<div id="foam-party" class="offset">
	<!--Start of Jumbotron -->
	<div class="fixed-background">
			<div class="row light no-padding">
				<div class="col-md-3">
					<div class="os-animation" data-animation="fadeInLeft">
						<img class="fn-img-align" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/TR_FoamParty/los-angeles-foam-party-totally-rad-logo-300x211.png" alt="We have Foam Parties - enquire now">
					</div>
				</div>
				<div class="col-md-6">
					<!-- Start Animation -->
					<div class="os-animation" data-animation="fadeInUp">
						<div class="narrow light text-center">
								<h3 class='heading'>Bubbling fun in yards of foam! A backyard party they’re sure to remember</h3>
								<div class="heading-underline"></div>
								<p class="lead">Our Foam parties are no hassle. Simply reserve your Foam Machine today. We’ll bring the equipment. Set it up and then be back at the end of your party to pick it all back up.<br>Having a community event or festival and need to cover a much bigger area? Reserve our Foam Machine!<br>There are a variety of locations that can work including grassy yards, parking lots, patios, sports courts, sandy areas, or poolside.</p>
								<a class="btn btn-secondary btn-sm" href="#contact">Get FOAM Today!</a>
						</div>
					</div>
					<!-- End Animation -->
				</div>
				<div class="col-md-3">
					<div class="os-animation" data-animation="fadeInRight">
						<img class="dark-shadow fn-img-align" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/TR_FoamParty/foamParty.jpeg" alt="We have Foam Parties - enquire now">
					</div>
				</div>
			</div>
		<div class="fixed-wrap">
			<div id="fixed-2">
			</div>
		</div>
	</div>
</div>

<!-- Start Pricing -->
<div id="pricing" class="offset">
		<div class="narrow padding">
			<div class="os-animation text-center" data-animation="fadeInUp">
				<h3 class="heading">Pricing</h3>
				<div class="heading-underline"></div>
				<p class="lead"><strong>TR to complete</strong>Pricing below are for our main services - pricing for the "<strong>two hour</strong>" period.</p>
			</div>

			<!-- Start of the Pricing Columns -->
			<div class="row justify-content-md-center">
				<div class="col-md-6 col-lg-4">
					<div class="os-animation " data-animation="fadeInLeft">
						<div class="pricing-column text-center light">
							<div class="lighter">
							<h3>LASER</br>TAG</h3>
							<p>Two hours of laser tag fun</p>
							<div class="pricing-features">
								<h4><span class="item">Laser Taggers</span>: 14</h4>
								<h4><span class="item">Head Visors</span>: 14</h4>
								<h4><span class="item">Respawn stations</span>: 2</h4>
								<h4><span class="item">Bunkers and HUBs</h4>
							</div>
							<h3 class="front-price">$350</h3>
							<a class="btn btn-secondary btn-sm" href="/booking">Book Now</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="os-animation" data-animation="fadeInUp">
						<div class="pricing-column text-center">
							<div class="lighter">
							<div class="ribbon">Super Popular</div>
							<h3>GAME</br>THEATRE</h3>
							<p>Bring your Party to life with our Video Game Theatre</p>
							<div class="pricing-features">
								<h4><span class="item">Multiplayer fun</span>: Up to 20</h4>
								<h4><span class="item">Latest Game Consoles</span></h4>
								<h4><span class="item">Mess free Parties</span></h4>
								<h4><span class="item">Fully supervised </h4>
							</div>
							<h3 class="front-price">$395</h3>
							<a class="btn btn-custcolor btn-sm" href="/booking">Book Now</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="os-animation" data-animation="fadeInRight">
						<div class="pricing-column text-center">
							<div class="lighter">
							<h3>VR</br>ADD-ON</h3>
							<p>Video games party with the immersion of VR</p>
							<div class="pricing-features">
								<h4><span class="item">Mobile Game Theatre</span></h4>
								<h4><span class="item">PS4 VR</span></h4>
								<h4><span class="item">More games variety</span></h4>
								<h4><span class="item">VR immersion</h4>
							</div>
							<h3 class="front-price">$465</h3>
							<a class="btn btn-secondary btn-sm" href="/booking">Book Now</a>
							</div>
						</div>
					</div>
				</div>


			</div>
			<!-- End of the Pricing Columns -->
			<div class="os-animation" data-animation="fadeInUp">
				<div class="narrow text-center">
					<div class="col-12">
						<p class="lead">Above pricing is our base prices for the majority of our parties (this does not include travel and fuel costs). If you need more time or interested in other services please visit our Pricing and Booking page</p>
						<a class="btn btn-secondary btn-sm" href="/booking">More Pricing</a>
					</div>
				</div>
			</div>	

		</div> <!-- End of Narrow -->
</div>
<!-- End Pricing  -->

<!-- Start Skills -->
<div id="skills" class="offset">	
	<!-- Start of Jumbotron -->
	<div class="jumbotron">
		<div class="narrow">
			<div class="col-12 os-animation" data-animation="fadeInUp">
				<h3 class="heading">Time to relax</h3>
				<div class="heading-underline"></div>
			</div>

			<div class="os-animation" data-animation="fadeInUp">
				<div class="row text-center">
					
					<div class="col-sm-6 col-md-3">
						<div class="skill">
							<span class="fa-layers fa-4x">
								<i class="fas fa-clock"></i>
							</span>
							<h3><span class="counter">120</span>+</h3>
							<p>Minutes of fun</p>
						</div>
					</div>

					<div class="col-sm-6 col-md-3">
						<div class="skill">
							<span class="fa-layers fa-4x">
								<i class="fab fa-xbox"></i>
							</span>
							<h3><span class="counter">150</span>+</h3>
							<p>Lastest Console games</p>
						</div>
					</div>	

					<div class="col-sm-6 col-md-3">
						<div class="skill">
							<span class="fa-layers fa-4x">
								<i class="fas fa-users" data-fa-transform="left-2"></i>
							</span>
							<h3><span class="counter">20</span>+</h3>
							<p>kids or adults playing</p>
						</div>
					</div>	

					<div class="col-sm-6 col-md-3">
						<div class="skill">
							<span class="fa-layers fa-4x">
								<i class="fas fa-cloud-download-alt" data-fa-transform="left-1"></i>
							</span>
							<h3><span class="counter">120</span>+</h3>
							<p>Minutes of down time</p>
						</div>
					</div>	

				</div><!-- End of Row-->
			</div><!-- End of Animation-->

			<div class="os-animation" data-animation="fadeInUp">
				<div class="narrow text-center">
					<div class="col-12">
						<p class="lead">Take the stress out of organising a stunning video game party or an amazing social gathering, and leave the entertaining to us.</p>
						<a class="btn btn-custcolor btn-sm" href="#contact">Contact Us</a>
					</div>
				</div>
			</div>

		</div><!--End of Narrow-->
	</div>
	<!-- End of Jumbotron -->
</div>
<!-- End Skills  -->

<!-- Start Team -->
<div id="video-game-part-information" class="offset">	
	<!--Start fixed background light-->
	<div class="fixed-background">
		<div class="dark">
		<div class="row dark-row">
			<div class="col-12 os-animation" data-animation="fadeInUp">
				<h3 class="heading">Kids Bus Party? Why not a kids game bus party!</h3>
				<div class="heading-underline"></div>
				<p class="lead">Some of the best VIDEO GAME PARTY IDEAS!</p>
			</div>
			<div class="col-md-6 col-lg-4">
				<div class="os-animation" data-animation="fadeInLeft">
					<div class="feature">
						<span class="fa-layers fa-4x">
							<i class="fa fa-circle"></i>
							<i class="fas fa-truck-monster fa-inverse" data-fa-transform="shrink-7 left-2"></i>
						</span>
						<h3>IT’S A LONG RIDE</h3>
						<p>Give us some space and we’ll give you a kids bus party! Don’t have the room? We suggest a city park, school oval or church lot as an alternative. Click <a href="/our-game-trailer/truck-trailer-parking/">here</a> for our “Parking Page”.</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4">
				<div class="os-animation" data-animation="fadeInUp">
					<div class="feature">
						<span class="fa-layers fa-4x">
							<i class="fa fa-circle"></i>
							<i class="far fa-heart fa-inverse" data-fa-transform="shrink-5 down-1"></i>
						</span>
						<h3>WE PROVIDE ENTERTAINMENT</h3>
						<p>Have an event that needs some unique entertainment, like a kids party bus perhaps? Book us for that amazing “Ghostly” party you’ve always wanted! Summer BBQ, Family Reunion, Halloween, New Years, Block Party, even Weddings. <a href="/booking">Let’s make it HAPPEN!</a></p>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4">
				<div class="os-animation" data-animation="fadeInRight">
					<div class="feature">
						<span class="fa-layers fa-4x">
							<i class="fa fa-circle"></i>
							<i class="fas fa-comment fa-inverse" data-fa-transform="shrink-7"></i>
						</span>
						<h3>SOCIAL GAMING WITH FRIENDS</h3>
						<p>They don’t just have fun. They have a blast! See more party pictures on our <a href="https://m.facebook.com/GhostlyGames">FACEBOOK</a> page! Please <strong>LIKE</strong> us while you are there.</p>
					</div>
				</div>
			</div>
		</div><!--End of Row light -->

		<div class="fixed-wrap">
			<div id="fixed-2" class="background-2">
			</div>
		</div>
	</div>
	</div>
	<!--End fixed background light-->
</div>
<!-- Start Clients -->
<div id="clients" class="offset">	

	<div class="container-fluid">
		<div class="row padding">
			<div class="col-12 os-animation" data-animation="fadeInUp">
				<h3 class="heading">Testimonials</h3>
				<div class="heading-underline"></div>
			</div>

			<div class="col-md-12">
				<div class="os-animation" data-animation="fadeInUp">
					<div id="clients-slider" class="owl-carousel owl-theme">
						
						<div class="row clients">
							<div class="col-md-4">
								<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/clients/Stacey_McDonald_family.jpg" alt="Stacey_McDonald_family">
							</div>
							<div class="col-md-8">
								<blockquote>
									<i class="fas fa-quote-left"></i>
									TR to complete
									<hr class="clients-hr"></hr>
									<cite>&#8212; Stacey McDonald</cite>
								</blockquote>
							</div>
						</div>
						<div class="row clients">
							<div class="col-md-4">
								<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/clients/Dani_Harnchan.jpg" alt="Dani Harnchan">
							</div>
							<div class="col-md-8">
								<blockquote>
									<i class="fas fa-quote-left"></i>
									 TR to complete
									<hr class="clients-hr"></hr>
									<cite>&#8212; Dani Harnchan</cite>
								</blockquote>
							</div>
						</div>
						<div class="row clients">
							<div class="col-md-4">
								<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/clients/Lisa_Cradock.jpg" alt="Lisa Cradock">
							</div>
							<div class="col-md-8">
								<blockquote>
									<i class="fas fa-quote-left"></i>
									TR to complete
									<hr class="clients-hr"></hr>
									<cite>&#8212; Lisa Cradock</cite>
								</blockquote>
							</div>
						</div>
						<div class="row clients">
							<div class="col-md-4">
								<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/clients/Mandy_Cowley.jpg" alt="Mandy Cowley">
							</div>
							<div class="col-md-8">
								<blockquote>
									<i class="fas fa-quote-left"></i>
									TR to complete
									<hr class="clients-hr"></hr>
									<cite>&#8212; Mandy Cowley</cite>
								</blockquote>
							</div>
						</div>
					</div><!-- End of Clients Slider -->
				</div><!-- End of animation -->
			</div><!--End of Col md 12 -->

		</div><!--End of Row -->

			<div class="os-animation" data-animation="fadeInUp">
				<div class="narrow text-center">
					<div class="col-12">
						<p class="lead">If the above was not enough to convince you we're awesome, come check out our facebook "reviews" for a lot more "convincing".</p>
						<a class="btn btn-custcolor btn-sm" href="https://www.facebook.com/totallyradgametruck/reviews/" target="_blank">Facebook Reviews</a>
					</div>
				</div>
			</div>
	</div><!-- End of Container -->

</div>
<!-- End Clients  -->


<?php get_footer(); ?>


