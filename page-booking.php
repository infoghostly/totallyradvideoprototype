<?php

get_header();

if(have_posts()) :
	while (have_posts()) : the_post(); ?>
		<article class="main-content-article">
				<?php get_template_part('nav-has-children');?>
				<div class="clearfix"></div>
				<!-- Start column-container Div -->
			<div class="main-content container-fluid entry-content-faq">
				<div class="narrow">
				<div class="index-p">
				<div class="col-12 os-animation remove-from-bookly" data-animation="fadeInUp">
					<h3 class="heading">Time to work out the Price</h3>
					<div class="heading-underline"></div>
					<p class="lead text-center">Welcome to the Pricing and Booking page. </br> Please select one (or more) of the services you wish to book from below. Once selected you will see your price below the services section. <strong>Please note:</strong> The VR service is an Add on service to the Mobile Game Theatre service and is not a seperate service.</p>
				</div>
				
				<!-- Start combination-pricing-contatiner Div -->
				<div class="combination-pricing-contatiner">
						
					<?php  /*BS cards below 
							First is the two main pricing*/?>
					<div class="row">
						<div class="col-md-6">
							<div class="os-animation" data-animation="fadeInUp">
							<div class="card text-center">
								<img class="card-img-top" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/team/MGT-NOVR-Team1.jpg" alt="Mobile Game Theatre">
								</img>
								<div class="card-body">
									<h4>Mobile Game Theatre</h4>
									<h5>Ghostly Fun in our custom built video gaming theatre.</h5>
									<div class="card-dropdown-container">
									    <div class="card-dropdown-toggle-MGT btn-custcolor-snd btn-sm" title="Dropdown">Package Includes:</div>
									    <ul class="card-dropdown text-left">
									     	<h3>Mobile Game Theatre</h3>
											<li>Multiplayer gaming experience</li>
											<li>Up to 20 players at once</li>
											<li>We bring the "Ghostly" fun to you - we're mobile</li>
											<li>Clean and mess free PARTIES</li>
											<li>All sorts of weather accepted - Rain, hail or shine</li>
											<li>4 Xbox One X, 4 PS4 Pro, 2 Nintendo Swtiches</li>
											<li>Fully supervised entertainment.</li>
									    </ul>
									</div>
									<input type="checkbox" id="mgt-checkbox" name="MGT" value="10" style="visibility: hidden;"></input>
									<div class="check-button">
										<label class="btn btn-secondary btn-sm" id="label-mgt-checkbox"  for="mgt-checkbox"></label>
									</div>
								</div>
							</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="os-animation" data-animation="fadeInUp">
							<div class="card text-center">
								<img class="card-img-top" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/team/LT-photos-Team3.jpg" alt="Laser Tag">
								</img>
								<div class="card-body">
									<h4>Laser Tag</h4>
									<h5>Serious tactical combat between each other in Laser Tag</h5>
									<div class="card-dropdown-container">
									    <div class="card-dropdown-toggle-LT btn-custcolor-snd btn-sm" title="Dropdown">Package Includes:</div>
									    <ul class="card-dropdown text-left">
									    	<h3>Laser Tag</h3>
											<li>14 Laser Taggers</li>
											<li>14 head visors for laser tag detection</li>
											<li>2 respawn stations</li>
											<li>2 HUB pop up stations</li>
											<li>6 pop up bunkers for added tactical hiding spots</li>
											<li>1 sergeant/game master to control the flow of the game - fully supervised</li>
									    </ul>
									</div>
									<input type="checkbox" id="lt-checkbox" name="LT" value="30" style="visibility: hidden;"></input>
									<div class="check-button">
											<label class="btn btn-secondary btn-sm" id="label-lt-checkbox"  for="lt-checkbox"></label>
									</div>
								</div>
							</div>
							</div>
						</div>
					</div>
					<div class="row">

						<div class="col-sm-6 col-md-4">
							<div class="os-animation" data-animation="fadeInLeft">
							<div class="card text-center">
								<img class="card-img-top" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/team/MGT-VR-Team1.jpg" alt="Mobile Game Theatre and Virtual Reality">
								</img>
								<div class="card-body">
									<h4>Virtual Reality Add On</h4>
									<h5>Add on - party with the immersion of VR</h5>
									<div class="card-dropdown-container">
									    <div class="card-dropdown-toggle-VR btn-custcolor-snd btn-sm" title="Dropdown">Package Includes:</div>
									    <ul class="card-dropdown text-left">
									      <h3>Virtual Reality</h3>
									      <li>PS4 Virtual Reality Head Set and Camera</li>
										  <li>Extra 10 VR games to play and choose from</li>
										  <li>Rotation control to ensure everyone in the party gets a go</li>
									    </ul>
									</div>
									<input type="checkbox" id="vr-checkbox" name="VR" value="20" style="visibility: hidden;"></input>
									<div class="check-button">
											<label class="btn btn-secondary btn-sm" id="label-vr-checkbox"  for="vr-checkbox"></label>
									</div>
								</div>
							</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="os-animation" data-animation="fadeInUp">
							<div class="card text-center">
								<img class="card-img-top" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/team/MGT-LT-SP-Team1.jpg" alt="Half Half Deal - Mobile Game Theatre and Laser Tag">
								</img>
								<div class="card-body">
									<h4>Special Half Half Deal</h4>
									<h5>Why not have both?</h5>
									<div class="card-dropdown-container">
									    <div class="card-dropdown-toggle-MGTSP btn-custcolor-snd btn-sm" title="Dropdown">Package Includes:</div>
									    <ul class="card-dropdown text-left">
											<h3>Half - Mobile Game Theatre</h3>
											<li>Multiplayer gaming experience</li>
											<li>Up to 20 players at once</li>
											<li>We bring the "Ghostly" fun to you - we're mobile</li>
											<li>Clean and mess free PARTIES</li>
											<li>All sorts of weather accepted - Rain, hail or shine</li>
											<li>4 Xbox One X, 4 PS4 Pro, 2 Nintendo Swtiches</li>
											<li>Fully supervised entertainment.</li>
											<hr></hr>
											<h3>Half - Laser Tag</h3>
											<li>14 Laser Taggers</li>
											<li>14 head visors for laser tag detection</li>
											<li>2 respawn stations</li>
											<li>2 HUB pop up stations</li>
											<li>6 pop up bunkers for added tactical hiding spots</li>
											<li>1 sergeant/game master to control the flow of the game - fully supervised</li>
									    </ul>
									</div>
									<input type="checkbox" id="mgt-lt-sp-checkbox" name="MGTLTSP" value="50" style="visibility: hidden;"></input>
									<div class="check-button">
											<label class="btn btn-secondary btn-sm" id="label-mgt-lt-sp-checkbox"  for="mgt-lt-sp-checkbox"></label>
									</div>
								</div>
							</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4 self-centre-md">
							<div class="os-animation" data-animation="fadeInRight">
							<div class="card text-center">
								<img class="card-img-top" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/team/DJ-Team2.jpg" alt="DJ Service">
								</img>
								<div class="card-body">
									<h4>DJ hire</h4>
									<h5>Hire an experienced DJ!</h5>
									<div class="card-dropdown-container">
									    <div class="card-dropdown-toggle-DJ btn-custcolor-snd btn-sm" title="Dropdown">Package Includes:</div>
									    <ul class="card-dropdown text-left">
									    	<h3>DJ Service</h3>
											<li>1 awesome DJ to rock your party</li>
											<li>Music selection of your choice - please contact us to confirm</li>
											<li>DJ Set up and Lighting</li>
									    </ul>
									</div>
									<input type="checkbox" id="dj-checkbox" name="DJ" value="40" style="visibility: hidden;"></input>
									<div class="check-button">
											<label class="btn btn-secondary btn-sm" id="label-dj-checkbox"  for="dj-checkbox"></label>
									</div>
								</div>
							</div>
							</div>
						</div>
					</div>

					<!-- Start combination-selection-services-hours Div -->
					<div class="combination-selection-services-hours">
						<div class="col-12 os-animation" data-animation="fadeInUp">
							<h3 class="heading">Select the amount of time</h3>
							<div class="heading-underline"></div>
							<p class="lead text-center">If you require our services longer than <strong>4 hours</strong> please don't hesitate and <a href="#contact">contact us</a> to organise a longer service.</p>
							<div class="input-group narrow narrow--20">
								<select class="custom-select price-combo-time-sel" required>
									<option value='1'> 1 hour </option>
									<option value='2'> 2 hours </option>
									<option value='3'> 3 hours </option>
									<option value='4'> 4 hours </option>
								</select>
							</div>
						</div>
					</div>
					<!-- End combination-selection-services-hours Div -->

					<!-- End combination-image-selections Div -->
					<!-- Start combination-pricing-outputs Div -->
					<!-- Start rib-price-cont Div -->
					<div class="rib-price-cont">
						<!-- Start combo-ribbon-main-price Div -->
						<div class="combo-ribbon-main-price jumbotron jumbotron-sm-padding">
							<div class="col-12 os-animation" data-animation="fadeInUp">
							<!-- Start ribbon-pos-cont Div -->
							<div class="ribbon-pos-cont">
								<!--<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/price-ribbon-blue.png" alt=""  border="0" class="ribbon-image-pricing">-->
								<h3 class="heading rib-price-text lead text-center">No Service Selected</h3>
								<div class="heading-underline"></div>

							</div>
							</div>
							<!-- End ribbon-pos-cont Div -->
						</div>
						<!-- Start combo-space Div -->
						<div class="combo-space">
						
						</div>
						<!-- End combo-space Div -->
						<!-- End combo-ribbon-main-price Div -->
						<div class="combination-pricing-outputs">
							<!-- Start combination-main-price Div -->
							<div class="combination-main-price">
								<p class="lead">Please choose a service. <strong>NOTE:</strong> Ensure you click/press on the "SELECT" button on the service card of your choice.</p>
							</div>
							<!-- End combination-main-price Div -->
							<!-- Start combination-main-features Div -->
							<div class="container-fluid jumbotron">
							<div class="combination-main-features">

							</div>
							</div>
							<!-- End combination-main-features Div -->
						</div>
						<!-- End combination-pricing-outputs Div -->
					</div>

										<!-- Start welcome-pricing Div -->
					<div class="container-fluid padding padding-bottom">
						<div class="important-notice policy-info-box policy-info-box--bookingPage">
						 <h4>IMPORTANT NOTICE</h4> <p>The above price <strong>may</strong> change if we need to incorporate additional travel surchages or Generator petrol costs. After your booking is placed, the Ghostly service coordinator will call to confirm your party location, advise  additional travel charges if any, and discuss power options to you. Please see our <a href="<?php echo get_site_url() ?>/our-game-trailer/our-service-area/">Service Area</a> to confirm location and generator petrol costs if warrented. </p>
						 <p>By Submitting Payment you agree to <a href="<?php echo get_site_url() ?>/about-us/terms-and-conditions/">Our Policies</a> and our <a href="<?php echo get_site_url() ?>/about-us/terms-and-conditions/deposit-and-cancellation-policy/">Deposit and Cancellation Policies</a></p>
						</div>
					</div>
					<p class="lead">To Continue with your booking, please press <strong>Next</strong> down below</p>
					<!-- End welcome-pricing Div -->	
				</div>
				<!-- End combination-pricing-contatiner Div -->
					<?php  the_content(); ?>
				</div>
				<!-- End index-p Div -->
			</div>
			</div>
		</article>
	<?php endwhile;
else :
	echo '<p>No content found </p>';
endif; 
?>

	<!-- Start MGT-ghost Div 
	<div class="MGT-ghost">
		<img class="service-ghosts mgtghost" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/small-MGT-ghost.png">
	</div>

	<div class="MGT-VR-ghost">
		<img class="service-ghosts vrghost" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/small-MGT-VR-ghost.png">
	</div>

	<div class="LT-ghost">
		<img class="service-ghosts ltghost" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/small-LT-ghost.png">
	</div>

	<div class="DJ-ghost">
		<img class="service-ghosts deejghost" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/small-DJ-ghost.png">
	</div>

	<div class="MGTLTSP-ghost">
		<img class="service-ghosts mgtltspghost" src="<?php echo esc_url(get_template_directory_uri()); ?>/img/smallHalfHalfGhosts.png">
	</div>
</div> -->
<!-- End serivce-ghosts-images-hidden Div -->

<?php get_footer();
?>