<?php

get_header();

if(have_posts()) :
	while (have_posts()) : the_post(); 
		get_template_part('content', get_post_format());
	endwhile;
else :
	echo '<p>No content found </p>';
endif;

  $authID = get_the_author_meta('ID');
  //echo $author->get('ID');
  // If comments are open or we have at least one comment, load up the comment template.
 if ( comments_open() || get_comments_number() ) :
     comments_template();
 endif;
 
  include( locate_template('author_info.php', false, false) );


get_footer();
?>