<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 */

get_header(); ?>

	<div id="primary" class="content-area container-fluid">
		<div id="content" class="site-content narrow" role="main">

			<div class="col-12 os-animation" data-animation="fadeInUp">
				<h3 class="heading">This is somewhat Embarrassing</h3>
				<div class="heading-underline"></div>
			</div>

			<div class="page-wrapper-error text-center">
				<div class="page-content">
					<p>It looks like nothing was found at this location. Please go back to <a href="/">home</a>. </p>

				</div><!-- .page-content -->
			</div><!-- .page-wrapper -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>