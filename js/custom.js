//This is to control the owl carousel
$(document).ready(function(){
	$("#team-slider").owlCarousel({
			items:3,
			autoplay:true,
			smartSpeed:700,
			loop:true,
			autoplayHoverPause:true,
			responsive : {
				0 : {
					items: 1
				},
				576 : {
					items: 2
				},
				768 : {
					items: 3
				}
			}
		}
		);
});

$(document).ready(function(){
	$("#clients-slider").owlCarousel({
			items:2,
			autoplay:true,
			smartSpeed:1700,
			loop:true,
			autoplayHoverPause:true,
			responsive : {
				0 : {
					items: 1
				},
				768 : {
					items: 2
				},
			}
		}
		);
});


/* For the counter plug in */

$(document).ready(function(){
	$('.counter').counterUp({
		delay: 10,
		time: 1800
	});
});