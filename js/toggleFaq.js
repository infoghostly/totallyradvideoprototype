//ToggleFAQ.js
$(document).ready(function() {
	setupchildtogglenumbers();
});

function setupchildtogglenumbers() {
	
//$('.entry-content-faq').each(function() {
//	var count = 0;
//	$(this).children('div').addClass('you_made_it_' + count); 
//	count ++;
//});

	var faqContent = document.getElementById("entry-content-faq-id");
	var children = faqContent.children;
	for (var i = 0; i < children.length; i++) {
		var faqSection = children[i];
		var faqChildren = faqSection.children;
		for (var b = 0; b < faqChildren.length; b++) {
			var testName = faqChildren[b].className;
			if (testName === 'qe-toggle-title' ) {
				faqChildren[b].className += " qe-toggle-title-tog_" + i;
			}
			else if (testName === 'qe-toggle-content'){
				faqChildren[b].className += " qe-toggle-content-tog_" + i;
			};
		}
		addfaqlisteners("_" + i);
	}
	
}

function checkothertogs(){
	var togglecheck = $('.hittog');
	if (togglecheck != undefined ) {
		togglecheck.slideToggle(200, function(){});
		togglecheck.removeClass('hittog');
	}
	var cirRmvTog = $('.hittogCr');
	cirRmvTog.removeClass('hittogCr');
}

function addfaqlisteners(num) {
	var faqbutton = $('.qe-toggle-title-tog' + num),
		faqAnswer = $('.qe-toggle-content-tog' + num);
	faqbutton.on('click', function(e){
		e.preventDefault();
		var imgCircle = $('.qe-toggle-title-tog' + num + ' .toggle-icon')
		var faqAText = faqAnswer.text();
		if(!faqAnswer.hasClass('hittog')){
			checkothertogs();
			faqAnswer.slideToggle(200, function(){
				$(this).addClass('hittog');
			});
			imgCircle.addClass('hittogCr')
		} else {
			checkothertogs();
		}
	});
};
