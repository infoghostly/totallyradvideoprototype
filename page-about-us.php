<?php

get_header();

if(have_posts()) :
	while (have_posts()) : the_post(); ?>
		<article class="container-fluid">
			<div class="narrow">
				<div class="row">
					<!-- Start title-column Div -->
					<div class="col-sm-3 os-animation" data-animation="bounce" data-delay=".1s">
						<img src="<?php echo esc_url(get_template_directory_uri()); ?>/img/icons/ghostlyGamesLogoShine.svg">
					</div>
					<!-- End title-column Div -->
					<!-- Start text-column Div -->
					<div class="col-sm-9">
						<!-- Start index-p Div -->
						<div class="index-p">
							<?php  the_content(); ?>
						</div>
						<!-- End index-p Div -->
					</div>
					<!-- End text-column Div -->
				</div>
				<!-- End column-container Div -->
		
			</div>
			<!-- End main-content Div -->
		</article>
	<?php endwhile;
else :
	echo '<p>No content found </p>';
endif; 
	//get_template_part('author_info');?>
<div id="all-authors-archive" class="jumbotron">
	<div class="col-12 os-animation" data-animation="fadeInUp">
		<h3 class="heading">Meet the Ghostly Family!</h3>
		<div class="heading-underline"></div>
		<p class="lead text-center">We take real pride with our family business. Look below to get to know us better.</p>
	</div>
	<?php
	// Arguments to pass to get_users
	$args = array( 'orderby' => 'ID', 'order' => 'ASC', 'who' => 'authors' );
	// Query for the users
	$authors = get_users( $args ); ?>
	 

	<?php
	/* Loop through all the users, then running the locate template to share the 
	 share the authID variable to the author_info.php template*/
	for ( $i = 0; $i < count( $authors ); ++$i ) {
	  $bTest = 	$i;
	  $author = $authors[$i];
	  $authID = $author->get('ID');
	  //echo $author->get('ID');
	  ?>
	  <div class="col-12 os-animation" data-animation="<?php if ($bTest % 2) {echo "bounceInLeft";} else {echo "bounceInRight";}; ?>">
	  <?php include( locate_template('author_info.php', false, false) ); ?>
	  </div>
	<?php } ?>
</div>
<?php 
get_footer();
?>